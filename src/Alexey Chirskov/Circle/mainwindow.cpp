﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}
MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_pbRadius_clicked()
{
    QString radius=ui->leRadius->text();
    MyCircle.setRadius(radius.toDouble());
    ui->leRadius->setText(QString::number(MyCircle.getRadius()));
    ui->leFerence->setText(QString::number(MyCircle.getFerence()));
    ui->leArea->setText(QString::number(MyCircle.getArea()));
}

void MainWindow::on_pbFerence_clicked()
{
    QString ference=ui->leFerence->text();
    MyCircle.setFerence(ference.toDouble());
    ui->leRadius->setText(QString::number(MyCircle.getRadius()));
    ui->leFerence->setText(QString::number(MyCircle.getFerence()));
    ui->leArea->setText(QString::number(MyCircle.getArea()));
}


void MainWindow::on_pbArea_clicked()
{
    QString area=ui->leArea->text();
    MyCircle.setArea(area.toDouble());
    ui->leRadius->setText(QString::number(MyCircle.getRadius()));
    ui->leFerence->setText(QString::number(MyCircle.getFerence()));
    ui->leArea->setText(QString::number(MyCircle.getArea()));
}
