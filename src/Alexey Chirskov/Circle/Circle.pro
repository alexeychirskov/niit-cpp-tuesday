#-------------------------------------------------
#
# Project created by QtCreator 2016-03-22T19:02:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Circle
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h\
        Circle.h

FORMS    += mainwindow.ui
