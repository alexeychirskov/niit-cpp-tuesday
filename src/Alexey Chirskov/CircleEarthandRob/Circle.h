#ifndef CIRCLE_H
#define CIRCLE_H
#include <math.h>

class Circle {
    double radius,ference,area;
public:
    Circle():radius(0),ference(0),area(0){}
    double getRadius(){return radius;}
    double getFerence(){return ference;}
    double getArea(){return area;}
    void setRadius(double r){
        radius=r;
        area=M_PI*r*r;
        ference=2*M_PI*r;
    }
    void setFerence(double f){
        radius=f/(2*M_PI);
        area=M_PI*(f/(2*M_PI))*(f/(2*M_PI));
        ference=f;
    }
    void setArea(double a){
        radius=sqrt(a/M_PI);
        area=a;
        ference=2*M_PI*sqrt(a/M_PI);
    }
};

#endif // CIRCLE_H
