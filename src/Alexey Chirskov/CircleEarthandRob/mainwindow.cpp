#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbNumber_clicked()
{
    Circle MyCircle;
    QString text=ui->leNumber->text();
    MyCircle.setRadius(N);
    double ference=MyCircle.getFerence()+text.toDouble()/1000;
    MyCircle.setFerence(ference);
    ui->leResult->setText(QString::number((MyCircle.getRadius()-N)*1000));
}
