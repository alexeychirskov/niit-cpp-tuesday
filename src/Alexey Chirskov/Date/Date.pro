#-------------------------------------------------
#
# Project created by QtCreator 2016-03-27T01:18:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Date
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    Date.h

FORMS    += mainwindow.ui
