#ifndef DATE_H
#define DATE_H
#define DAY_SEC 86400
#include<time.h>
typedef unsigned int UI;
class Date {
    struct tm * ptime;
    UI year, daymonth, day, weekday;
    time_t t;
    char * weekdays[7] = {"Воскресенье", "Понедельник","Вторник",
                          "Среда","Четверг", "Пятница", "Суббота"};
    char * month[12] = {"Январь", "Февраль","Март","Апрель","Май","Июнь",
                        "Июль","Август","Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};
public:
    void printToday()
    {
        time(&t);
        ptime=localtime(&t);
    }
    void printYesterday()
    {
        time(&t);
        t-=DAY_SEC;
        ptime=localtime(&t);
    }
    void printTomorrow()
    {
        time(&t);
        t+=DAY_SEC;
        ptime=localtime(&t);
    }
   void printFuture(int number)
   {
       time(&t);
       t+=DAY_SEC*number;
       ptime=localtime(&t);
   }
   void printPast(int number)
   {
       time(&t);
       t-=DAY_SEC*number;
       ptime=localtime(&t);
   }
    int calcDifference(int day1, int month1, int year1, int day2, int month2, int year2)
    {
       int one, two;
       time(&t);
       ptime=localtime(&t);
       ptime->tm_year=year1-1900;
       ptime->tm_mon=month1-1;
       ptime->tm_mday=day1;
       one=mktime(ptime);
       ptime->tm_year=year2-1900;
       ptime->tm_mon=month2-1;
       ptime->tm_mday=day2;
       two=mktime(ptime);
       return (two-one)/DAY_SEC;
    }
    int getYear(){year=ptime->tm_year;return year+1900;}
    int getDay(){day=ptime->tm_mday;return day;}
    char * getWeekDay(){weekday=ptime->tm_wday;return weekdays[weekday];}
    char * getMonth(){daymonth=ptime->tm_mon;return month[daymonth];}
};

#endif // DATE_H
