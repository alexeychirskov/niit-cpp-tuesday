#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include"Date.h"
#include<QMainWindow>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbToday_clicked();
    void on_pbYesterday_clicked();
    void on_pbTomorrow_clicked();
    void on_pbFurture_clicked();
    void on_pbPast_clicked();
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    Date MyDate;
};

#endif // MAINWINDOW_H
