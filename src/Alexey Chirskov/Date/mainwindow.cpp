#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbToday_clicked()
{
    MyDate.printToday();
    ui->leDay->setText(QString::number(MyDate.getDay()));
    ui->leMonth->setText(MyDate.getMonth());
    ui->leWeekDay->setText(MyDate.getWeekDay());
    ui->leYear->setText(QString::number(MyDate.getYear()));
}

void MainWindow::on_pbYesterday_clicked()
{
    MyDate.printYesterday();
    ui->leDay->setText(QString::number(MyDate.getDay()));
    ui->leMonth->setText(MyDate.getMonth());
    ui->leWeekDay->setText(MyDate.getWeekDay());
    ui->leYear->setText(QString::number(MyDate.getYear()));
}

void MainWindow::on_pbTomorrow_clicked()
{
    MyDate.printTomorrow();
    ui->leDay->setText(QString::number(MyDate.getDay()));
    ui->leMonth->setText(MyDate.getMonth());
    ui->leWeekDay->setText(MyDate.getWeekDay());
    ui->leYear->setText(QString::number(MyDate.getYear()));
}

void MainWindow::on_pbFurture_clicked()
{
    QString number=ui->leFuture->text();
    MyDate.printFuture(number.toInt());
    ui->leDay->setText(QString::number(MyDate.getDay()));
    ui->leMonth->setText(MyDate.getMonth());
    ui->leWeekDay->setText(MyDate.getWeekDay());
    ui->leYear->setText(QString::number(MyDate.getYear()));
}

void MainWindow::on_pbPast_clicked()
{
    QString number=ui->leFuture->text();
    MyDate.printPast(number.toInt());
    ui->leDay->setText(QString::number(MyDate.getDay()));
    ui->leMonth->setText(MyDate.getMonth());
    ui->leWeekDay->setText(MyDate.getWeekDay());
    ui->leYear->setText(QString::number(MyDate.getYear()));

}

void MainWindow::on_pushButton_clicked()
{
    QString DateOneDay=ui->leDataOneDay->text();
    QString DateOneMonthDay=ui->leDataOneMonthDay->text();
    QString DateOneYear=ui->leDataOneYear->text();
    QString DateTwoDay=ui->leDataTwoDay->text();
    QString DateTwoMonthDay=ui->leDataTwoMonthDay->text();
    QString DateTwoYear=ui->leDataTwoYear->text();
    ui->leFuture->setText(QString::number(MyDate.calcDifference(DateOneDay.toInt(),DateOneMonthDay.toInt(),
                    DateOneYear.toInt(), DateTwoDay.toInt(),DateTwoMonthDay.toInt(),DateTwoYear.toInt())));
}
