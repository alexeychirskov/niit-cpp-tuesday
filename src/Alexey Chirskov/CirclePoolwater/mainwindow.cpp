#include "mainwindow.h"
#include "ui_mainwindow.h"
Circle MyCircle;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbButton_clicked()
{
    QString radius=ui->leRadius->text();
    QString width=ui->leWidth->text();
    MyCircle.setRadius(radius.toDouble());
    double ference=MyCircle.getFerence();
    double area=MyCircle.getArea();
    MyCircle.setRadius(radius.toDouble()+width.toDouble());
    area=MyCircle.getArea()-area;
    ui->leResult->setText(QString::number(ceil(area)*1000+ceil(ference)*2000));
}
