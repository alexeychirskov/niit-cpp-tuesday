#-------------------------------------------------
#
# Project created by QtCreator 2016-03-25T14:26:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CirclePoolwater
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h\
        Circle.h

FORMS    += mainwindow.ui
