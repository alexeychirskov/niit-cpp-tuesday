#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <math.h>
#include <QMainWindow>
#include "Circle.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbButton_clicked();

private:
    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
